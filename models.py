from peewee import *
from flask_login import UserMixin

database = SqliteDatabase("fluxcagnachelecart.sqlite3")

class BaseModel(Model):
    class Meta:
        database = database

class Users(BaseModel, UserMixin): 
    email = CharField(unique=True)
    login = CharField(unique=True)
    password = CharField()

class Flows(BaseModel):
    userID = BigIntegerField()
    flowURL = CharField()

def create_tables():
    with database:
        database.create_tables([Users,Flows ])
        print("Creation tables")

def drop_tables():
    with database:
        database.drop_tables([Users, Flows ])
        print("Drop tables")
